import argparse
import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

def main(args):
    try:
        duty_cycle = args.NT / (args.nt+ args.NT)
        print(duty_cycle)
        t = np.linspace(0, 1, 500, endpoint=False)
        plt.ylim(-2, 2)
        signal_list = signal.square(2 * np.pi * 5 * t, duty=duty_cycle)
        plt.plot(t, signal_list)
        plt.show()
    except KeyboardInterrupt as k:
        plt.close('all') 


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate periodic pulse')
    parser.add_argument('--nt', type=int, required=True)
    parser.add_argument('--NT', type=int, required=True)
    args = parser.parse_args()
    main(args)
